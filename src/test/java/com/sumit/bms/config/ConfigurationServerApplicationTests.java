package com.sumit.bms.config;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author imsumit (Sumit Karmakar)
 * @since Apr 21, 2024
 */
@SpringBootTest
class ConfigurationServerApplicationTests {

	@Test
	void contextLoads() {
	}

}
